package com.example.easynotes.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity // persistent java class
@Table(name = "notes")
@EntityListeners(AuditingEntityListener.class) // created and last modified will auto-update
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
		allowGetters = true) // Jackson annotation; SB uses Jackson for de/serializing JO to/from JSON; ignores supplied values for these two values
public class Note implements Serializable{

	@Id //primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY) // primary key generator
	private Long id;
	
	@NotBlank // not null
	private String title;
	
	@NotBlank
	private String content;
	
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP) // used with Date and Calendar to be converted from JO to comparable db type
	@CreatedDate
	private Date createdAt; // will become created_at in db
	
	@Column(nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
}
