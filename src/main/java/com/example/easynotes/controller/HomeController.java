package com.example.easynotes.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping(value="/jquery")
	public String jquery() {
		return "jquery-ex.html";
	}
	
	@RequestMapping(value="/angularjs")
	public String angularjs() {
		return "angularjs-ex.html";
	}
}
