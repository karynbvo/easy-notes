/**
 * 
 */
var url = "http://localhost:8080/api/notes";
$(document).ready(function() {
    $.ajax({
        url: url,
        method: "GET"
    }).then(function(data) {
       $.each(data, function(index, item){
    	   console.log(`Data is: ${item.title}`);
    	  $("tbody").append(`<tr id="dataItem-${item.id}">
						    	  <td class="title"><input type="text" value=\"${item.title}\"></input></td>
						    	  <td class="content"><input type="text" value=\"${item.content}\"></input></td>
						    	  <td><button onclick="saveItem(${item.id})" class="button">Save</button></td>
					    	  </tr>`);
       });
    });
});

function saveItem(id){
	console.log("id: " + id);
	
	var dataItem = new Object();
	dataItem.title = $("#dataItem-" + id + " .title input").val();
	dataItem.content = $("#dataItem-" + id + " .content input").val();
	
	console.log(dataItem);
	
	$.ajax({
		url: `${url}/${id}`,
		method: "PUT",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(dataItem),
		success: function(data, textStatus, xhr){
			console.log(data);
		},
		error: function(xhr, textStatus, errorThrown){
			console.log(errorThrown);
		}
	});
}

function addNewItem(){
	$("tbody").append(`<tr id="dataItem-${item.id}">
	    	  <td class="title"><input type="text" value=\"${item.title}\"></input></td>
	    	  <td class="content"><input type="text" value=\"${item.content}\"></input></td>
	    	  <td><button onclick="saveItem(${item.id})" class="button">Save</button></td>
  	  		</tr>`);
}