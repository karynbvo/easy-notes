var url = "http://localhost:8080/api/notes";

angular.module('angularjs-ex', [])
	.controller('NotesController', function($scope, $http){
		
		$http.get(url)
			.then(function(response){
				$scope.items = response.data;
			});
		
		$scope.updateItem = function(item){
			var id = item.id;
			$http.put(`${url}/${id}`, JSON.stringify(item))
				.then(function(result, error){
					if(error)
						console.log(error);
					else
						console.log(result);
				});
		};
		
		$scope.addNewRow = function(){
			$scope.items.push({
				"title": "",
				"content": ""
			}); 
			console.log($scope.items);
		};
	});